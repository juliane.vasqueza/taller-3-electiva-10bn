package com.ecci.taller3


import android.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.ecci.taller3.adapter.ProductAdapter
import com.ecci.taller3.adapter.ProductProtocol
import com.ecci.taller3.dialogs.ProductDialog
import com.ecci.taller3.model.Product
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), ProductProtocol {


    //Lateinit me permite asegurarle a
    // Android que esta variable sera inicializada más adelante
    private lateinit var adapter: ProductAdapter
    var productsToRemove: MutableList<Product> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupButtons()
        setupList()

    }

    private fun setupButtons() {
        addProductButton.setOnClickListener {
            val dialog = ProductDialog(this, "", "", 0,false) { name, description, stock->
                addProduct(name, description, stock)
            }
            dialog.show()
        }
        removeProductsButton.setOnClickListener {
            if (productsToRemove.isEmpty()){
                Toast.makeText(this, "Seleccione los productos que desea eliminar", Toast.LENGTH_SHORT).show()
            }
            else{
                val builder = AlertDialog.Builder(this)
                builder.setTitle("Confirmación")
                builder.setMessage("¿Esta seguro que quiere eliminar los productos seleccionados?")
                builder.setPositiveButton("Si"){_,_ ->
                    var removedProducts: MutableList<Product> = mutableListOf(Product("","",0,false))
                    for (product in productsToRemove){
                        deleteProduct(product)
                        removedProducts.add(product)
                    }
                    for (product in removedProducts){
                        productsToRemove.remove(product)
                    }
                }
                builder.setNegativeButton("Cancelar"){_,_ ->
                }
                builder.show()

            }
        }
    }

    private fun setupList() {
            val products : MutableList<Product> = mutableListOf()
            adapter = ProductAdapter(products, this) { item, isDelete ->
            if(isDelete) deleteProduct(item)
            else editProduct(item)
        }
        myProductRecyclerView.adapter = adapter
        myProductRecyclerView.layoutManager = LinearLayoutManager(this)
    }

    private fun addProduct(name: String, description: String, stock: Int) {
        val product = Product(name, description, stock, false)
        adapter.addProduct(product)
        adapter.notifyDataSetChanged()
    }

    private fun deleteProduct(product: Product) {
        //Crear una confirmación
        adapter.deleteProduct(product)
        adapter.notifyDataSetChanged()


    }

    fun updateProduct(product: Product, name: String, description: String, stock: Int) {

        adapter.editProduct(product, name, description, stock)
        adapter.notifyDataSetChanged()
    }

    private fun editProduct(product: Product) {
        val dialog = ProductDialog(this, product.name, product.description, product.stock,true) { name, description, _ ->
            updateProduct(product, name, description, product.stock)
        }
        dialog.show()
    }

    override fun didProductSelect(product: Product) {
        adapter.notifyDataSetChanged()
    }

    override fun productsToDelete(product: Product, isToDelete: Boolean) {
        if(isToDelete){
            productsToRemove.add(product)
        }else{
            productsToRemove.remove(product)
        }
    }

    override fun hideActivtyText(products: MutableList<Product>) {
        if (products.isEmpty()){
            activityText.visibility = View.VISIBLE
        }else{
            activityText.visibility = View.GONE
        }
    }

}
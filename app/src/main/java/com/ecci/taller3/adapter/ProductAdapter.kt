package com.ecci.taller3.adapter

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import androidx.recyclerview.widget.RecyclerView
import com.ecci.taller3.R
import com.ecci.taller3.model.Product
import kotlinx.android.synthetic.main.item_product.view.*

class ProductAdapter(val products: MutableList<Product>, var delegate: ProductProtocol, val callback: (Product, Boolean) -> Unit): RecyclerView.Adapter<ProductAdapter.ProductViewHolder>() {

    class ProductViewHolder(itemView: View, var delegate: ProductProtocol, val callback: (Product, Boolean) -> Unit) : RecyclerView.ViewHolder(itemView) {
        private lateinit var adapter: ProductAdapter

        @SuppressLint("ResourceAsColor", "UseCompatLoadingForDrawables")
        fun bind(item: Product, products:  MutableList<Product>) {
            delegate.hideActivtyText(products)
            itemView.nameTextView.text = item.name
            itemView.descriptionTextView.text = item.description
            itemView.numberProductTextView.text = item.stock.toString()
            itemView.deleteButton.setOnClickListener {
                val builder = AlertDialog.Builder(itemView.context)
                builder.setTitle("Confirmación")
                builder.setMessage("¿Esta seguro que quiere eliminar el producto?")
                builder.setPositiveButton("Si"){_,_ ->
                    callback(item, true)
                }
                builder.setNegativeButton("Cancelar"){_,_ ->
                }
                builder.show()
            }

            itemView.editButton.setOnClickListener {
                callback(item, false)
            }

            //ImageButton para disminuir el stock
            itemView.lessButton.setOnClickListener{
               //Solo se va a disminuir si es estock wa mayor a cero
                if(item.stock >0){
                   adapter = ProductAdapter(products, delegate) { item, isDelete ->
                   }
                   adapter.editProduct(item, item.name, item.description, (item.stock)-1)
                   itemView.numberProductTextView.text = item.stock.toString()
                   adapter.notifyDataSetChanged()
               }
            }
            //ImageButton para aumentar el stock el stock
            itemView.moreButton.setOnClickListener {
                adapter = ProductAdapter(products, delegate) { item, isDelete ->

                }
                adapter.editProduct(item, item.name, item.description, (item.stock)+1)
                itemView.numberProductTextView.text = item.stock.toString()
                adapter.notifyDataSetChanged()
            }

            itemView.setOnClickListener {
                item.isSelelcted = !item.isSelelcted

                delegate.didProductSelect(item)

            }

            if(item.isSelelcted){
                itemView.setBackgroundColor(itemView.resources.getColor(R.color.clearRed))
                delegate.productsToDelete(item, true)
            }else{
                itemView.setBackgroundColor(itemView.resources.getColor(R.color.clearGreen))
                delegate.productsToDelete(item, false)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_product, parent, false)
        return ProductViewHolder(view,  delegate,callback)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        holder.bind(products[position], products)
    }

    override fun getItemCount(): Int {
        return products.size
    }

    fun addProduct(product: Product) {
        products.add(product)
    }

    fun deleteProduct(product: Product) {
        products.remove(product)
    }



    fun editProduct(product: Product, newName: String, newDescription: String, newStock: Int) {
        val index = products.indexOf(product)
        products[index].name = newName
        products[index].description = newDescription
        products[index].stock = newStock
    }

}

interface ProductProtocol {
    fun didProductSelect(pruduct: Product)
    fun productsToDelete(pruduct: Product, isToDelete: Boolean)
    fun hideActivtyText(products: MutableList<Product>)
}
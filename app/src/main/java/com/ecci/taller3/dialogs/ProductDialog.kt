package com.ecci.taller3.dialogs

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.core.view.isGone
import androidx.core.view.isVisible
import com.ecci.taller3.R
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.dialog_product.*

class ProductDialog(context: Context, val name: String, val description: String, val stock: Int, val isModifier: Boolean, private val callback: (String, String, Int) -> Unit) : Dialog(context) {
    @SuppressLint("ResourceAsColor")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_product)
        nameEditText.setText(name)
        descriptionEditText.setText(description)
        if(isModifier){
            stockEditText.visibility = View.GONE
            dialogTitle.setText("Modificar Producto")
            stockEditText.setText(stock.toString())
        }
        saveButton.setOnClickListener {
            //Validación para que todos los campos sean diligenciados
            if (nameEditText.text.isEmpty() || descriptionEditText.text.isEmpty() || stockEditText.text.toString().isEmpty()){
                dialogError.setText("Todos los campos son obligatorios")
                dialogError.visibility = View.VISIBLE
            }
            else{
                val name = nameEditText.text.toString()
                val description = descriptionEditText.text.toString()
                if(!isModifier){
                    val stock = stockEditText.text.toString().toInt()
                    callback(name, description, stock)
                }
                else{
                    callback(name, description, stock)
                }
                dismiss()
            }

        }
    }
}
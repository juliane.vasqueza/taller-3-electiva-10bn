package com.ecci.taller3.model

data class Product(var name: String, var description: String, var stock: Int, var isSelelcted: Boolean)